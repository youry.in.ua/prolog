:- use_module(library(pce)).

col(black, white).
col(white, black).

main(_) :-
    free(@ic),
    free(@p),

    abolish(m/3),
    abolish(p/3),
    abolish(c/2),
    
    D is 36,
    N is 15,

    gen_c(x, 1, N), 
    gen_c(y, 1, N),

    XC is (N + 1) / 2,
    YC is (N + 1) / 2,

    new(@p, dialog('Gomoku')),

    findall(_, gen_p(), _),

    M1 = m(1, black, p(XC, YC)),
    set_m(M1),
    
    draw(N, D),

    send(@p, open).

on_click(_, P, N, D) :-
    get(P, x, MX),
    get(P, y, MY),
    X is (MX + D / 2) // D,
    Y is (MY + D / 2) // D,
    between(1, N, X),
    between(1, N, Y),
    max_m(V),
    NN is V + 1,
    M = m(NN, white, p(X, Y)),
    set_m(M),
    draw_m(D, NN),
    NM is NN + 1,
    calc(NM, MM),
    set_m(MM),
    draw_m(D, NM).

calc(NN, M) :-
    p(X, Y, free),
    M = m(NN, black, p(X, Y)). 

set_m(M) :-
    M = m(_, C , p(X, Y)),
    P = p(X, Y, free),
    retract(P),
    assertz(p(X, Y, C)),
    assertz(M).

max_m(V) :-
    findall(NM, m(NM, _, _), L),
    max_list(L, V).

draw(N, D) :-
    L is D * (N + 1),
    new(@ic, device),

    send(@ic, recogniser,
        click_gesture(left, '', single, 
            message(@prolog, on_click, @receiver, @event?position, N, D)
        )
    ),
    send(@ic, display, box(L, L)),
    findall(_, draw_grid_v(D, L), _),
    findall(_, draw_grid_h(D, L), _),
    findall(_, draw_game(D), _),
    send(@p, display, @ic, point(20, 20)).

draw_grid_v(D, L) :- 
    c(x, X),
    X1 is X * D,
    Y1 is 0,
    X2 is X * D,
    Y2 is L,
    send(@ic, display, line(X1, Y1, X2, Y2)).

draw_grid_h(D, L) :- 
    c(y, Y),
    X1 is 0,
    Y1 is Y * D,
    X2 is L,
    Y2 is Y * D,
    send(@ic, display, line(X1, Y1, X2, Y2)).

draw_game(D) :- 
    m(N, _, _),
    draw_m(D, N).

draw_m(D, N) :-
    m(N, C, p(X, Y)),
    col(C, CT),
    XF is X * D - D / 2,
    YF is Y * D - D / 2,
    new(F, circle(D)),
    new(T, text(N)),
    get(T, width, TW),
    get(T, height, TH),
    XT is X * D - D / 2 + (D - TW) / 2,
    YT is Y * D - D / 2 + (D - TH) / 2,
    send(F, fill_pattern, colour(C)),
    send(T, colour, CT),
    send(@ic, display, F, point(XF,YF)),
    send(@ic, display, T, point(XT, YT)).

gen_c(D, N, N) :-
    assertz(c(D, N)). 
gen_c(D, N1, N2) :-
    assertz(c(D, N1)), 
    M is N1 + 1,
    gen_c(D, M, N2).

gen_p() :- 
    c(x, X),
    c(y, Y),
    P = p(X, Y, free),
    assertz(P).



